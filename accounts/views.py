from django.shortcuts import render, redirect

from django.contrib.auth import login
from django.contrib.auth.forms import UserCreationForm

# Create your views here.


def signup(request):
    # run request (when user makes a request on the signup page)
    if request.method == 'POST':
        # UserCreationForm is automatically generated from import
        form = UserCreationForm(request.POST)
        # if the form inputs are valid
        if form.is_valid():
            # newly create form is saved to user
            user = form.save()
            # login will be passed after request is passed
            # user is the form that was just created and saved
            login(request, user) # this line basically means, log in the user that just made this request and create the account
            # returns the user to the home page
            return redirect('home')
    # if they're just visiting this page, they're just directed to the signup page
    else:
        form = UserCreationForm()

    return render(request, 'registration/signup.html', {'form': form})
